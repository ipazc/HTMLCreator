/**
 * TreeController unit v0.3
 * by Iván de Paz Centeno
 * 06/04/2015 
 */

/**
 * Tree Controller class.
 * Represent a controller which handles the tree events.
 * Allows us to serialize the board components inside a tree.
 */
function TreeController(pBoard, pTree)
{
    var __board = pBoard;
    var __tree = pTree;
    var __treeContent = null;
    var __initialized = false;
    var __onSelectNode = null;
    var __dontNotify = false;
    
    
    /**
     * Private function to generate the tree contents
     */
    function __generateTreeWay()
    {
        if ($(this).data('name'))
        {
            __treeContent += '<li id="lid_'+$(this).data('name')+'">' + $(this).data('name');        
            __treeContent += '<ul>';
            $(this).children().each(__generateTreeWay);
            __treeContent += '</ul>';
            __treeContent += '</li>';
        }
    }
    
    /**
     * Sets the callback function to event OnSelectNode.
     */
    this.setOnSelectNode = function(pEventFunc)
    {
        __onSelectNode = pEventFunc;
    }
    
    /**
     * Generates the tree and returns it wrapped inside a JQuery element.
     */
    this.generateTree = function()
    {
        __treeContent = '<ul>';
        __board.children().each(__generateTreeWay);
        __treeContent += '</ul>';
        __treeContent = $(__treeContent);
        return __treeContent;
    }
    
    /**
     * Selects the tree element corresponding to the specified board element
     */
    this.selectTreeElement = function(pBoardElement)
    {
        if (!pBoardElement || !__initialized)
        {
            return;
        }
        
        var lTreeElementName = "lid_"+pBoardElement.data('name');
        __dontNotify = true;
        __tree.jstree("deselect_all");
        __tree.jstree('select_node', lTreeElementName);
        this.focusOnSelection();
    }
    
    /**
     * Scrolls the bars to focus on the selection
     */
    this.focusOnSelection = function()
    {
        var lSelectedNode = __tree.jstree('get_selected');
        if (lSelectedNode && lSelectedNode.length > 0)
        {
            var lToScroll = $( "#"+lSelectedNode[0] ).offset().left;
            document.getElementById( lSelectedNode[0] ).scrollIntoView();        
            __tree.scrollLeft(__tree.scrollLeft() + lToScroll);
            lToScroll = $( "#"+lSelectedNode[0] ).offset().left;   
            __tree.scrollLeft(__tree.scrollLeft() + lToScroll);
        }
    }
    
    /**
     * Generates and updates the tree visually.
     */
    this.updateTree = function ()
    {
        var lPreviouslySelectedNode = null;
        
        if (__initialized)
        {
            lPreviouslySelectedNode = __tree.jstree('get_selected');
        }
        
        __tree.jstree("destroy");
        if (__treeContent)
        {
            __treeContent.remove();
        }
        
        __tree.append(this.generateTree());
       
        // listen for event
        __tree.on('select_node.jstree', function (pEvent, pData) 
        {
            var lSelectedObj = $("#"+pData.node.id.substring(4));
            
            if (__onSelectNode && !__dontNotify)
            {
                __onSelectNode(lSelectedObj);
            } else
            {
                __dontNotify = false;   
            }
        })
        
        __tree.jstree();
        __initialized = true;
        
        if (lPreviouslySelectedNode && lPreviouslySelectedNode.length > 0)
        {
            __dontNotify = true;
            __tree.jstree('select_node', lPreviouslySelectedNode[0]);
            this.focusOnSelection();
        }
    }
    
}