/**
 * Mouse unit v0.3
 * by Iván de Paz Centeno
 * 03/04/2015 
 */

/**
 * Package Mouse
 */
//Mouse = {
    
    /**
     * Mouse singleton class.
     * Represents the mouse.
     * Allows to retrieve mouse coords (absolute and/or
     * relative).
     */
    var Mouse = (function () 
    {
        var __instance;
        var __mouseEventInstance;
        function createInstance(pEvent) 
        {
            return new MouseClass(pEvent);
        }
        
        return {
            getMouseHandler: function(pEvent)
            {
                if (!__instance)
                {
                    __instance = createInstance(pEvent);
                }
                else
                {
                    __instance.wrapEvent(pEvent);
                }
                
                return __instance;
            }
        };
    }) ();
    
    /**
     * Class that manages the mouse.
     */
    function MouseClass(pEvent)
    {
        var __defaultEvent = new MouseEvent();
        var __event;
        
        /**
         * Wraps an event to be handled by the mouse.
         * If no event is passed as an argument, default mouse event
         * will be taken
         */
        this.wrapEvent = function(pEvent)
        {
            if (pEvent)
            {
                __event = pEvent;
            }
            else
            {
                __event = __defaultEvent;
            }
        }
        
        /**
         * Retrieves the absolute coordenates (x and y) from the whole
         * document (if not parameter is specified) or the coordenates relative to a given holder. Even when scrollbars are activated.
         */
        this.getCoords = function(pHolder)
        {
            var lLeft = __event.pageX + $(document).scrollLeft();
            var lTop = __event.pageY + $(document).scrollTop();
            
            if (pHolder)
            {
                lLeft = lLeft - pHolder.offset().left + pHolder.scrollLeft();
                lTop = lTop - pHolder.offset().top + pHolder.scrollTop();
            }
            
            return new Position(lLeft, lTop);
        };
        
        /**
         * Retrieves the distance between the mouse and the element left, top
         */
        this.getDistanceBetweenMouseAnd = function(pElement)
        {
            if (!pElement)
                return null;
            
            var lMousePos = this.getCoords();
            var lElementPos = new Position(pElement.offset().left, pElement.offset().top);
            
            return new Position (lMousePos.GetX() - lElementPos.GetX(), 
                                 lMousePos.GetY() - lElementPos.GetY());
            
        }
        
        /**
         * Retrieves the distance between the mouse and the element width, height
         */
        this.getDistanceToEndBetweenMouseAnd = function (pElement)
        {
            if (!pElement)
                return null;
            
            var lMousePos = this.getCoords();
            var lElementEndPos = new Position(pElement.offset().left + pElement.width(), pElement.offset().top + pElement.height());
            
            return new Position (lElementEndPos.GetX() - lMousePos.GetX(), 
                                 lElementEndPos.GetY() - lMousePos.GetY());
            
        }

        /**
         * Returns if the event is produced by the right click
         */
        this.isRightMB = function()
        {
            var lIsRightMB = false;
            
            if ("which" in __event)  // Gecko (Firefox), WebKit (Safari/Chrome) & Opera
                lIsRightMB = __event.which == 3; 
            else if ("button" in __event)  // IE, Opera 
                lIsRightMB = __event.button == 2; 
            
            return lIsRightMB;
        }
        
        /**
         * Returns the coordenates relative to the visible window area.
         */
        this.getVisibleCoords = function()
        {
            return new Position(__event.pageX, __event.pageY);
        };
        
        
        
        this.wrapEvent(pEvent);
    }
    
    
    /**
     * Class that represent a basic default mouse event.
     * Holds the mouse coords for each movement and handles the
     * mouse events
     */
    function MouseEvent()
    {
        var __self = this;
        
        $(document).bind('mousemove', function(e){
            __self.pageX = e.pageX;
            __self.pageY = e.pageY;
        });
    }
    
    
    /**
     * Class that holds a 2D position (x and y coords).
     */
    function Position(pX, pY)
    {
        var __X = pX;
        var __Y = pY;
        
        this.GetX = function()
        {
            return __X;
        };
        
        this.GetY = function()
        {
            return __Y;
        };
    }
//}