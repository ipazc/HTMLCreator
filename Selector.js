/**
 * Selector unit v0.3
 * by Iván de Paz Centeno
 * 03/04/2015 
 */


/**
 * Class that handles the element selections, the element movements and resizements.
 */
function Selector(pBoard, pHighlighter, pHinter, pTreeController, pTableModifier, pPropertiesManager)
{
    var __selectedElement = null;
    var __highlighter = pHighlighter;
    var __board = pBoard;
    var __hinter = pHinter;
    var __treeController = pTreeController;
    var __tableModifier = pTableModifier;
    var __propertiesManager = pPropertiesManager;
    
    var __onlyMove = false;
    
    var __gridSize = 8;
    var __initializeMovement = false;
    
    var __resizingProperties = [];
    
    /**
     * Holds the HTML dots that processes the resizing effect.
     */
    var __resizeDots = [];
    
    /**
     * Holds the distance between the mouse and the top-left.
     */
    var __initialPosition = null;
    
    /**
     * Holds the distance between the mouse and the width-height.
     */
    var __initialSizePosition = null;
    
    /**
     * Updates the grid size of the board
     */
    this.updateGridSize = function (pGridSize)
    {
        __gridSize = pGridSize;
    }
    
    /**
     * Clears the selection from the current selected item
     */
    this.clearSelection = function ()
    {
        if (!this.isAnyElementSelected())
        {
            return;
        }
        
        __selectedElement.removeClass('selectedItem');
        __selectedElement = null;
        __initializeMovement = false;
        __resizingProperties = [];
        __tableModifier.disableModification();            
        
        // Also, the dots must be removed from the DOM.
        for (i = 0; i < __resizeDots.length; i++)
        {
            __resizeDots[i].remove();
        }
        
        __resizeDots = [];
    }
    
    /**
     * Determines whether an element is actually selected or not.
     */
    this.isAnyElementSelected = function ()
    {
        return __selectedElement != null;
    }
    
    /**
     * Selects the given element (marking it in a different colour and spawning some dots to allow resizing)
     */
    this.selectElement = function (pElement)
    {   
        if (!pElement || pElement.attr('id') == 'hintElementTop' || pElement.attr('id') == 'hintElementBottom')
        {
            this.clearSelection();
            return;
        }
     
         
        if (this.isAnyElementSelected() && __selectedElement == pElement)
        {
            return;
        }
        
        if (pElement.data("tableHandler") == "true")
        {
            return;
        }
        
        this.clearSelection();
        
        __selectedElement = pElement;
        
        
        
        
        // If it's a table, we allow modifications
        if (__selectedElement.data("type") == "table")
        {
            __tableModifier.wrapTable(__selectedElement);
            __tableModifier.enableModification();
        }
        else
        {
            __tableModifier.disableModification();            
        }
        
        if (__selectedElement.data("resizable") != "false")
        {
            //DONE: Spawn 8 dots and store them in __resizeDots array.
            var lTopLeftDot = $("<div id=\"topLeftDot\" class=\"resizeDot\"></div>");
            var lTopRightDot = $("<div id=\"topRightDot\" class=\"resizeDot\"></div>");
            var lBottomLeftDot = $("<div id=\"bottomLeftDot\" class=\"resizeDot\"></div>");
            var lBottomRightDot = $("<div id=\"bottomRightDot\" class=\"resizeDot\"></div>");
            var lTopDot = $("<div id=\"topDot\" class=\"resizeDot\"></div>");
            var lBottomDot = $("<div id=\"bottomDot\" class=\"resizeDot\"></div>");
            var lLeftDot = $("<div id=\"leftDot\" class=\"resizeDot\"></div>");
            var lRightDot = $("<div id=\"rightDot\" class=\"resizeDot\"></div>");
            
            __resizeDots = [ lTopLeftDot, lTopRightDot, lBottomLeftDot, lBottomRightDot, lTopDot, lLeftDot, lRightDot, lBottomDot ];
        
            //Now we need to append them to the selected element.
            for (i=0; i< __resizeDots.length; i++)
            {
                __selectedElement.append(__resizeDots[i]);
            }
            
            //Now we need to configure the dots positions:
            this.distributeResizeDots();
        }
        
        //Let's notify the tree to select the corresponding tree element.
        __treeController.selectTreeElement(__selectedElement);
        
        
        //We need to notify to the properties manager which element is being selected before we set the selected class.
        __highlighter.cancelHightlight();
        __propertiesManager.setElementBeingEdited(__selectedElement);
        __highlighter.highlightElement(__selectedElement);
        
        __selectedElement.addClass('selectedItem');
    }
    
    /**
     * Deletes the selected element from the DOM
     */
    this.deleteSelection = function()
    {
        if (!this.isAnyElementSelected()) 
            return;
        
        if (__selectedElement.data("deletable") == "false")
            return;
        
        __selectedElement.remove();
        
        __selectedElement = null;
        __initializeMovement = false;
        __resizingProperties = [];
        
        // Also, the dots must be removed from the DOM.
        for (i = 0; i < __resizeDots.length; i++)
        {
            __resizeDots[i].remove();
        }
        
        __resizeDots = [];
        __treeController.updateTree();
    }
    
    /**
     * Determines whether the selected element is being resized or not.
     */
    this.isBeingResized = function()
    {
        return this.isAnyElementSelected() && __resizingProperties.length > 0;
    }
    
    /**
     * Distributes the resize dots over the selected item corners.
     */
    this.distributeResizeDots = function()
    {
        if (__resizeDots.length == 0)
        {
            return;
        }
        
        var lMiddleDotSize = __resizeDots[0].width() / 2;
        var lMiddleWidth = __selectedElement.width() / 2;
        var lMiddleHeight = __selectedElement.height() / 2;
        
        
        __resizeDots[0].css({
            top: -lMiddleDotSize,
            left: -lMiddleDotSize,
        });
        
        __resizeDots[1].css({
            top: -lMiddleDotSize,
            left: __selectedElement.width() - lMiddleDotSize,
        });
        
        __resizeDots[2].css({
            top: __selectedElement.height() - lMiddleDotSize,
            left: - lMiddleDotSize,
        });
        
        __resizeDots[3].css({
            top: __selectedElement.height() - lMiddleDotSize,
            left: __selectedElement.width() - lMiddleDotSize
        });
        
        __resizeDots[4].css({
            top: - lMiddleDotSize,
            left: lMiddleWidth - lMiddleDotSize
        });
        
        __resizeDots[5].css({
            top: lMiddleHeight - lMiddleDotSize,
            left: - lMiddleDotSize
        });
        
        __resizeDots[6].css({
            top: lMiddleHeight - lMiddleDotSize,
            left: __selectedElement.width() - lMiddleDotSize
        });
        
        __resizeDots[7].css({
            top: __selectedElement.height() - lMiddleDotSize,
            left: lMiddleWidth - lMiddleDotSize
        });
    }
    
    /**
     * Determines whether this selector will handle only movements or movements+resizements.
     */
    this.setOnlyMove = function(pOnlyMove)
    {
        __onlyMove = pOnlyMove;
    }
    
    /**
     * Finds the dot index inside the array from the given element
     */
    this.getIndexOfDot = function(pDotElement)
    {
        if (!pDotElement)
            return -1;
        
        
        for (i =0; i < __resizeDots.length; i++)
        {
            if (__resizeDots[i].attr('id') == pDotElement.attr('id'))
                return i;
        }
        
        return -1;
    }
    
    /**
     * Given a dot index, this method returns an array of strings containing the properties that must be
     * modified to accomplish the resizing.
     */
    this.translateResizeDotToProperty = function(pDotIndex)
    {
        //To guide:         __resizeDots = [ lTopLeftDot, lTopRightDot, lBottomLeftDot, lBottomRightDot, lTopDot, lLeftDot, lRightDot, lBottomDot ];
        
        var lProperties = [];
        
        switch (pDotIndex)
        {
            case 0: 
                lProperties = ['top', 'left', 'width', 'height' ];
                break;
            case 1:
                lProperties = ['top', 'width', 'height'];
                break;
            case 2:
                lProperties = ['height', 'left', 'width'];
                break;
            case 3:
                lProperties = ['height', 'width'];
                break;
            case 4:
                lProperties = ['top', 'height'];
                break;
            case 5:
                lProperties = ['left', 'width'];
                break;
            case 6:
                lProperties = ['width'];
                break;
            case 7:
                lProperties = ['height'];
                break;
        }
        
        return lProperties;
    }
    
    /**
     * Event raised whenever a mouse down event is fired
     */
    this.mouseDown = function (pMouse)
    {
        //3 cases:
        //   (1) One element is actually selected and highlighter is over it: we must control the movement of the element.
        //   (2) Highlighter is over some element (not the actually selected): mark the highlighted element as selected.
        //   (3) Highlighter is over a resizing dot: treat the movement as a resize.
        
        var lHighlightedElement;
        var lActualElement =__highlighter.getActualElement(pMouse);
        
        if (__onlyMove)
        {
            lHighlightedElement = __selectedElement;
        }
        else
        {
            lHighlightedElement = lActualElement;
        }
        
        
        if (lActualElement && lActualElement.data("tableHandler") == "true")
        {
            return;
        }
        
        if (!this.isAnyElementSelected() || lHighlightedElement != __selectedElement)
        {
            var lDotIndex = this.getIndexOfDot(lHighlightedElement);
            
            
            //if lDotIndex > -1 means that a resizing dot has been pressed.
            if (lDotIndex > -1)
            {
                __resizingProperties = this.translateResizeDotToProperty(lDotIndex);
                __initialPosition = pMouse.getDistanceBetweenMouseAnd (__selectedElement);
                __initialSizePosition = pMouse.getDistanceToEndBetweenMouseAnd (__selectedElement);
                
                __hinter.showHint(__selectedElement);
                
                
                //To avoid highlights while resizing.
                __highlighter.finishHighlightOfElements(true);
                return;
            }
            else
            {
                // Let's select the element.
                this.selectElement(lHighlightedElement);
            }
        } 
        
        if (this.isAnyElementSelected() && (__selectedElement.data("movable") != "false"))
        {
            //DONE: Initialize the movement of the element.
            __initializeMovement = true;
            __initialPosition = pMouse.getDistanceBetweenMouseAnd (__selectedElement);
            __initialSizePosition = pMouse.getDistanceToEndBetweenMouseAnd (__selectedElement);
            
            //To avoid highlights while moving.
            if (!__onlyMove)
            {
                __highlighter.finishHighlightOfElements(true);
            }
            
            if (pMouse.isRightMB())
            {
                if (__hinter.isAnyElementHinted() && __hinter.getHintedElement().attr('id') == __selectedElement.attr('id'))
                {
                    __hinter.clearHint();
                }
                else
                {
                    __hinter.showHint(__selectedElement);
                }
            }
            else
            {
                __hinter.showHint(__selectedElement);
            }
            
            
        }
    }
    
    /**
     * Event raised whenever a mouse move event is fired
     */
    this.mouseMove = function (pMouse)
    {
        if (pMouse.isRightMB())
        {
            return;
        }
     
        
        
        
        if (this.isAnyElementSelected())
        {
            var lElement;
            
            if (__selectedElement.data('type') == "tdcontent")
            {
                //The tdcontent is a content wrap to allow resizements of the td (and the correct position of the dots).
                lElement = __selectedElement.parent();
            } 
            else
            {
                lElement = __selectedElement;
            }
            
            if (__initializeMovement || (this.isBeingResized() && !__onlyMove))
            {
                //1. We retrieve the new distance.
                var lNewDistance = pMouse.getDistanceBetweenMouseAnd (lElement);
                var lNewEndDistance = pMouse.getDistanceToEndBetweenMouseAnd (lElement);
                
                //Since distance between mouse and element must keep constant, let's calculate the differences.
                var lLeftDifference = getNearestValueToMultipleOf (lNewDistance.GetX() - __initialPosition.GetX(), __gridSize);
                var lTopDifference = getNearestValueToMultipleOf (lNewDistance.GetY() - __initialPosition.GetY(), __gridSize);
                var lWidthDifference = getNearestValueToMultipleOf (lNewEndDistance.GetX() - __initialSizePosition.GetX(), __gridSize);
                var lHeightDifference = getNearestValueToMultipleOf (lNewEndDistance.GetY() - __initialSizePosition.GetY(), __gridSize);
            }
            //var lUpdateInitialPosition = false;
            
            if (__initializeMovement)   // MOVEMENT OF THE ELEMENT.
            {
                
                var lLeft;
                var lTop;
                
                lLeft = getNearestValueToMultipleOf(lElement.position().left + lElement.parent().scrollLeft() + lLeftDifference, __gridSize);
                lTop = getNearestValueToMultipleOf(lElement.position().top + lElement.parent().scrollTop() + lTopDifference, __gridSize);
                
                //Let's update the position of the element.
                lElement.css({
                    left: lLeft,
                    top: lTop
                });
                
                this.distributeResizeDots();
                
            }
            else if (this.isBeingResized() && !__onlyMove) // RESIZE OF THE ELEMENT.
            {
                var lNewValue;
                var lLastValue;
                
                // Let's read all the values and modify them with the desired difference.
                var lProperties = __resizingProperties;
                var lMustUpdateInitialPosition = false;
                var lTopModified = false;
                var lLeftModified = false;
                
                for (i = 0; i < lProperties.length; i++)
                {
                    if (lProperties[i] == "top")
                    {
                        lNewValue = lElement.position().top + lElement.parent().scrollTop() + lTopDifference;
                        lTopModified = true;
                    }
                    else if (lProperties[i] == "left")
                    {
                        lNewValue = lElement.position().left + lElement.parent().scrollLeft() + lLeftDifference;
                        lLeftModified = true;
                    }
                    else if (lProperties[i] == "width")
                    {
                        if (lLeftModified)
                        {
                            lNewValue = lElement.width() - lLeftDifference;
                        }
                        else
                        {
                            lNewValue = lElement.width() - lWidthDifference;
                        }
                    }
                    else if (lProperties[i] == "height")
                    {
                        if (lTopModified)
                        {
                            lNewValue = lElement.height() - lTopDifference;
                        }
                        else
                        {
                            lNewValue = lElement.height() - lHeightDifference;
                        }
                    }
                    
                    
                    if ((Math.abs(lTopDifference) > __gridSize) || (Math.abs(lLeftDifference) > __gridSize)
                        ||(Math.abs(lWidthDifference) > __gridSize) || (Math.abs(lHeightDifference) > __gridSize))                        
                    {
                        lNewValue = getNearestValueToMultipleOf(lNewValue, __gridSize);
                        lElement.css(lProperties[i], lNewValue);
                    }
                    
                    
                }
                
                this.distributeResizeDots();
            }
            
            // Hinter mouse move event to update the hints.
            if (__hinter.isAnyElementHinted())
            {
                __hinter.onMouseMove(pMouse);
            }
            
        }
    }
    
    /**
     * Event raised whenever a mouse up event is fired
     */
    this.mouseUp = function (pMouse)
    {
        if (__initializeMovement)
        {
            __initializeMovement = false;
            
            if (!__onlyMove)
            {
                __highlighter.initHighlightOfElements();
            }
            
            __propertiesManager.updateSizeAndPosition();
        }
        
        if (this.isBeingResized())
        {
            __resizingProperties = [];
            __highlighter.initHighlightOfElements();
            
            if (!__onlyMove)
            {
                __highlighter.setHighlightedElement(__selectedElement);
            }
            
            __propertiesManager.updateSizeAndPosition();
        }
        
        if (!pMouse.isRightMB())
        {
            __hinter.clearHint();
        }
    }
}