/**
 * Tool unit v0.3
 * by Iván de Paz Centeno
 * 03/04/2015 
 */

/**
 * Package Tools
 */
    
var ToolTypes = Object.freeze({
    "CreateDiv":0, 
    "CreateTable":1, 
    "CreateParagraph":2
});

/**
 * Returns the nearest value to a multiple of pMultiple given a pValue
 */
function getNearestValueToMultipleOf(pValue, pMultiple)
{
    
    var lMultipleMiddle = pMultiple / 2;
    var lMultipleValueMod = pValue % pMultiple;
    
    var lToSum;
    
    if (lMultipleValueMod - lMultipleMiddle > -1)
        lToSum = pMultiple - lMultipleValueMod;  
    else
        lToSum = -lMultipleValueMod;
    
    
    return pValue + lToSum;
}



/**
 * Class that handles the tools.
 */
function ToolManager(pBoard, pTree, pProps)
{
    var __self = this;
    var __board = pBoard;
    var __props = pProps;
    var __highlighter = new Highlighter(pBoard);
    var __hinter = new Hinter(pBoard);
    var __selectedTool = null;
    var __selectedToolIcon = null; //html object that represents the tool icon
    var __gridSize = 8;
    var __treeController = new TreeController(pBoard, pTree);
    var __tableModifier = new TableModifier(pBoard, null, __treeController);
    var __propertiesManager = new PropertiesManager(pBoard, pProps);
    var __selector = new Selector(pBoard, __highlighter, __hinter, __treeController, __tableModifier, __propertiesManager);
    
    
    var __mouse = Mouse.getMouseHandler ();
    
    var __mouseCoordsEvent = null;
    
    /**
     * Tools definitions
     */
    var __tools = [
        new DivTool(pBoard, __highlighter, __hinter, __treeController),
        new TableTool(pBoard, __highlighter, __hinter, __treeController, __tableModifier)
    ];
    
    /**
     * Initializes the board body
     */
    this.createBoardBody = function()
    {
        var lBody = $("<div id=\"Body\" class=\"abs\"></div>");
        lBody.data("name","Body");
        lBody.data("movable","false");
        lBody.data("resizable","false");
        lBody.data("deletable","false");
        lBody.css("background","rgba(0,0,0,0)");
        if (__board[0].scrollWidth)
        {
            lBody.width(__board[0].scrollWidth);
        }
        else
        {
            lBody.width(__board.width());
        }
        if (__board[0].scrollHeight)
        {
            lBody.height(__board[0].scrollHeight);
        }
        else
        {
            lBody.height(__board.height());            
        }
        
        return lBody;
    }
    
    /**
     * Autoadjusts the body size to the board.
     */
    this.autoAdjustBodySize = function(pEvent)
    {
        if (__board[0].scrollWidth)
        {
            $("#Body").width(__board[0].scrollWidth);
        }
        else
        {
            $("#Body").width(__board.width());            
        }
        if (__board[0].scrollHeight)
        {
            $("#Body").height(__board[0].scrollHeight);
        }
        else
        {
            $("#Body").height(__board.height());            
        }
    }
    
    /**
     * Called whenever a tree node is selected
     */
    this.onTreeNodeSelected = function (pElement)
    {
        __selector.selectElement(pElement);
    }
    
    
    
    /**
     * Updates the grid size of the board
     */
    this.updateGridSize = function (pGridSize)
    {
        __gridSize = pGridSize;
        __selector.updateGridSize(pGridSize);
        
        for (i=0; i < __tools.length; i++)
        {
            __tools[i].updateGridSize(pGridSize);
        }
    }
    
    /**
     * Sets the mousemove event notifier to the given function.
     * For each mouse move event, the notifier will be called with a string
     * representing the coords relative to the board.
     */
    this.setMouseCoordsNotifier = function (pMouseCoordsNotifierFunc)
    {
        __mouseCoordsEvent = pMouseCoordsNotifierFunc;
    };
    
    /**
     * Getter for the selected tool.
     */
    this.getSelectedTool = function () 
    {
        return __selectedTool;
    };
    
    /**
     * Selects the given tool index and attachs its icon to the mouse.
     * Check Tools superclass to know
     * the valid indexes.
     */
    this.selectTool = function(pToolIndex)
    {
        var lTool = __tools[pToolIndex];
        
        if (__selectedTool == lTool)
        {
            return null;
        }
        
        __self.clearElementSelection();
        __self.clearToolSelection();
        
        // We request the tool icon to the tool.
        // The tool Icon is the icon that follows the cursor while moving.
        __selectedTool = lTool;
        __selectedToolIcon = lTool.getToolIcon();
        
        // Now we attach the tool to the view to make it visible.
        $("body").append(__selectedToolIcon);

        __selectedToolIcon.css({
            left:  __mouse.getVisibleCoords().GetX() - 5,
            top:   __mouse.getVisibleCoords().GetY() - 5
        });
        
        return __selectedTool;
    };
    
    /**
     * Method that determines whether a tool is actually selected or not
     */
    this.isToolSelected = function()
    {
        return __selectedTool != null;
    };
    
    /**
     * method that clears the tool selection.
     */
    this.clearToolSelection = function()
    {
        if (!__self.isToolSelected())
        {
            return;
        }
        
        __selectedToolIcon.remove();
        __selectedToolIcon = null;
        __selectedTool.cancelCreation();
        __selectedTool = null;
        this.clearElementSelection();
        
        __highlighter.initHighlightOfElements();
    };
    
    
    /**
     * Selects the given element. This wraps the element inside 4 dots and changes the element in order
     * to know that it's selected.
     */
    this.selectElement = function(pElement)
    {
        __selector.selectElement(pElement);
    }
    
    /**
     * Clears the selection.
     */
    this.clearElementSelection = function()
    {
        __selector.clearSelection();
    }
    
    //Now we define some events
    pBoard.mousedown(function(e)
    {
        __mouse.wrapEvent(e);
        
        if (__self.isToolSelected())
        {
            // Bubble the event down to the tool.
            __selectedTool.toolMouseDown(__mouse);
        }
        else
        {
            //selector must take care of clicks if no tools are chosen.
            __selector.mouseDown(__mouse);
        }
    });
    
    $(document).bind('mousemove', function(e)
    {
        __mouse.wrapEvent(e);

        // The highligher must know the element on which we are right now.
        __highlighter.mouseMove(__mouse);
        
        // If there is an element chosen, we should make things with it.
        __selector.mouseMove(__mouse);
        
        if (__self.isToolSelected())
        {   
            __selectedToolIcon.css({
                left:  __mouse.getVisibleCoords().GetX() + 5,
                top:   __mouse.getVisibleCoords().GetY() + 5
            });

            // And also the event bubble must go down to the tool.
            __selectedTool.toolMouseMove(__mouse);
        }
        
        
        var lX = __mouse.getCoords(__board).GetX();
        var lY = __mouse.getCoords(__board).GetY();
        
        // We also want to notify the caller with the mouse coords to show somewhere on the page.
        if (__mouseCoordsEvent && lX >= 0 && lY >= 0)
        {
            __mouseCoordsEvent("[X: " + parseInt(lX) + ", Y: " + parseInt(lY)+"]");
        }
    });
    
    $(document).bind('mouseup', function(e)
    {
        __mouse.wrapEvent(e);

        if (__self.isToolSelected())
        {
            // Bubble the event down to the tool.
            __selectedTool.toolMouseUp(__mouse);
        }
        else
        {
            //selector must take care of clicks.
            __selector.mouseUp(__mouse);
        }
        
        __board.trigger($.Event('resize'));
    });
    
    $(document).keydown(function(pEvent){
        switch (pEvent.keyCode) 
        {
            case 16: //MAYUS KEY
                __selector.setOnlyMove(true);
                __board.css("cursor","move");
                __highlighter.finishHighlightOfElements(false);
                break;
        }
    });
    
    $(document).keyup(function(pEvent){
        
        switch (pEvent.keyCode) 
        {
            case 16: //MAYUS KEY
                __board.css("cursor","crosshair");
                __highlighter.initHighlightOfElements();
                __selector.setOnlyMove(false);
                break;
            case 27: //ESC KEY
                __self.clearToolSelection();
                break;
            case 46: //DELETE KEY
                var lFocused = $(':focus').attr('id');
                if (!lFocused || lFocused.split('_')[0] == "lid")
                {
                    __selector.deleteSelection();
                }
                break;
        }
    });
    
    __board.bind('resize',this.autoAdjustBodySize);
    __treeController.setOnSelectNode(this.onTreeNodeSelected);
    
    
    
    __board.append(this.createBoardBody());
    __treeController.updateTree();
    
    __highlighter.initHighlightOfElements();
}

