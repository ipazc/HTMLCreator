/**
 * Highlighter unit v0.3
 * by Iván de Paz Centeno
 * 03/04/2015 
 */

/**
 * Handles the element highlighting control of the tools.
 */
function Highlighter(pBoard)
{
    var __board = pBoard;
    var __lastHighlightedElement = null;
    var __mustHighlightElements = false;
    
    /**
     * Makes this hightlighter to begin the process of highlight the elements.
     */
    this.initHighlightOfElements = function()
    {
        __mustHighlightElements = true;
    };
    
    /**
     * Makes this hightlighter to stop the process of highlight the elements.
     */
    this.finishHighlightOfElements = function(pKeepActualHighlighted)
    {
        __mustHighlightElements = false;
        
        if (!pKeepActualHighlighted)
        {
            this.cancelHightlight();
        }
    };
    
    /**
     * Returns true if any element of the board is hightlighted.
     */
    this.isAnyElementHighlighted = function()
    {
        return __lastHighlightedElement != null;
    };
    
    /**
     * Returns the current highlighted element.
     */
    this.getHightlightedElement = function()
    {
        return __lastHighlightedElement;
    };
    
    /**
     * Setter for the last highlighted element
     */
    this.setHighlightedElement = function (pElement)
    {
        __lastHighlightedElement = pElement;
    };
    
    /**
     * Brings the element highlighted back to a normal state.
     */
    this.cancelHightlight = function()
    {
        if (!this.isAnyElementHighlighted())
        {
            return;
        }
        
        __lastHighlightedElement.removeClass("highlighted");
        __lastHighlightedElement = null;
    };
    
    /**
     * Highlights the desired element
     */
    this.highlightElement = function(pElement)
    {
        if (this.isAnyElementHighlighted())
        {
            __lastHighlightedElement.removeClass("highlighted");
        }
        
        pElement.addClass("highlighted");
        __lastHighlightedElement = pElement;
    };
    
    /**
     * If cursor are not over the element... shouldn't be highlighted.
     */
    this.getActualElement = function(pMouse)
    {
        var lOverElement = $(document.elementFromPoint(pMouse.getVisibleCoords().GetX(), pMouse.getVisibleCoords().GetY()));
        
        if (lOverElement.parents('#'+__board.attr('id')).length)
            return lOverElement;
        
        return null;
    }
    
    /**
     * Event fired on mouse move
     */
    this.mouseMove = function(pMouse)
    {
        
        if (!__mustHighlightElements)
        {
            return;
        }
        
        var lOverElement = this.getActualElement(pMouse);
        
        if (lOverElement && lOverElement.attr('id') != 'hintElementTop' && lOverElement.attr('id') != 'hintElementBottom')
        {
            //Let's highlight it.
            if (!lOverElement.hasClass("selectedItem"))
                this.highlightElement(lOverElement);
        }
        else
        {
            this.cancelHightlight();
        }
        
    };
    
}
