/**
 * PropertiesManager unit v0.1
 * by Iván de Paz Centeno
 * 04/05/2015 
 */

/**
 * Properties manager class.
 * Represent a manager for css properties of a given element.
 */
function PropertiesManager(pBoard, pPropertiesSection)
{
    var __propertiesSection = pPropertiesSection;
    var __splitter = pPropertiesSection.find('#propValueSplitter');
    var __board = pBoard;
    var __elementEdited = null;
    var __genericProperties = [
        ['background-attachment', 'select', 'scroll|fixed|local|initial|inherit'],
        ['background-color', 'normal', ''],
        ['background-image', 'normal', ''],
        ['background-position', 'dynamicSelect', 'initial|inherit|left top|left center|left bottom|right top|right center|right bottom|center top|center center|center bottom'],
        ['background-position-x', 'normal', ''],
        ['background-position-y', 'normal', ''],
        ['background-repeat', 'select', 'repeat|repeat-x|repeat-y|no-repeat|initial|inherit'],
        ['border-bottom-color', 'normal', ''],
        ['border-bottom-style', 'select', 'none|hidden|dotted|dashed|solid|double|groove|ridge|inset|outset|initial|inherit'],
        ['border-bottom-width', 'normal', ''],
        ['border-collapse', 'select', 'separate|collapse|initial|inherit'],
        ['border-color', 'normal', ''],
        ['border-left-color', 'normal', ''],
        ['border-left-style', 'select', 'none|hidden|dotted|dashed|solid|double|groove|ridge|inset|outset|initial|inherit'],
        ['border-left-width', 'normal', ''],
        ['border-right-color', 'normal', ''],
        ['border-right-style', 'select', 'none|hidden|dotted|dashed|solid|double|groove|ridge|inset|outset|initial|inherit'],
        ['border-right-width', 'normal', ''],
        ['border-spacing', 'normal', ''],
        ['border-style', 'select', 'none|hidden|dotted|dashed|solid|double|groove|ridge|inset|outset|initial|inherit'],
        ['border-top-color', 'normal', ''],
        ['border-top-style', 'select', 'none|hidden|dotted|dashed|solid|double|groove|ridge|inset|outset|initial|inherit'],
        ['border-top-width', 'normal', ''],
        ['border-width', 'normal', ''],
        ['border-top-left-radius', 'normal', ''],
        ['border-top-right-radius', 'normal', ''],
        ['border-bottom-left-radius', 'normal', ''],
        ['border-bottom-right-radius', 'normal', ''],
        ['bottom', 'normal', ''],
        ['box-shadow', 'normal', ''],
        ['color', 'normal', ''],
        ['cursor', 'select', 'alias|all-scroll|auto|cell|context-menu|col-resize|copy|crosshair|default|e-resize|ew-resize|grab|grabbing|help|move|n-resize|ne-resize|nesw-resize|ns-resize|nw-resize|nwse-resize|no-drop|none|not-allowed|pointer|progress|row-resize|s-resize|se-resize|sw-resize|text|URL|vertical-text|w-resize|wait|zoom-in|zoom-out|initial|inherit'],
        ['display', 'select', 'inline|block|flex|inline-block|inline-flex|inline-table|list-item|run-in|table|table-caption|table-column-group|table-header-group|table-footer-group|table-row-group|table-cell|table-column|table-row|none|initial|inherit'],
        ['direction', 'select', 'ltr|rtl|initial|inherit'],
        ['float', 'select', 'left|right|none|inherit'],
        ['font-family', 'dynamicSelect', 'Georgia, serif|"Palatino Linotype", "Book Antiqua", Palatino, serif|"Times New Roman", Times, serif|Arial, Helvetica, sans-serif|"Arial Black", Gadget, sans-serif|"Comic Sans MS", cursive, sans-serif|Impact, Charcoal, sans-serif|"Lucida Sans Unicode", "Lucida Grande", sans-serif|Tahoma, Geneva, sans-serif|"Trebuchet MS", Helvetica, sans-serif|Verdana, Geneva, sans-serif|"Courier New", Courier, monospace|"Lucida Console", Monaco, monospace'],
        ['font-size', 'dynamicSelect', 'medium|xx-small|x-small|small|large|x-large|xx-large|smaller|larger|initial|inherit'],
        ['font-size-adjust', 'normal', ''],
        ['font-stretch', 'select', 'ultra-condensed|extra-condensed|condensed|semi-condensed|normal|semi-expanded|expanded|extra-expanded|ultra-expanded|initial|inherit'],
        ['font-style', 'select', 'normal|italic|oblique|initial|inherit'],
        ['font-variant', 'select', 'normal|small-caps|initial|inherit'],
        ['font-weight', 'dynamicSelect', 'normal|bold|bolder|lighter|initial|inherit'],
        ['height', 'normal', ''],
        ['left', 'normal', ''],
        ['letter-spacing', 'normal', ''],
        ['line-height', 'normal', ''],
        ['margin-bottom', 'normal', ''],
        ['margin-left', 'normal', ''],
        ['margin-right', 'normal', ''],
        ['margin-top', 'normal', ''],
        ['max-height', 'normal', ''],
        ['max-width', 'normal', ''],
        ['min-height', 'normal', ''],
        ['min-width', 'normal', ''],
        ['outline-color', 'normal', ''],
        ['outline-style', 'select', 'none|hidden|dotted|dashed|solid|double|groove|ridge|inset|outset|initial|inherit'],
        ['outline-width', 'normal', ''],
        ['overflow-X', 'select', 'visible|hidden|scroll|auto|initial|inherit'],
        ['overflow-Y', 'select', 'visible|hidden|scroll|auto|initial|inherit'],
        ['padding-bottom', 'normal', ''],
        ['padding-left', 'normal', ''],
        ['padding-right', 'normal', ''],
        ['padding-top', 'normal', ''],
        ['position', 'select', 'static|absolute|fixed|relative|initial|inherit'],
        ['right', 'normal', ''],
        ['scrollbar-arrow-color', 'normal', ''],
        ['scrollbar-base-color', 'normal', ''],
        ['scrollbar-dark-shadow-color', 'normal', ''],
        ['scrollbar-face-color', 'normal', ''],
        ['scrollbar-highlight-color', 'normal', ''],
        ['scrollbar-shadow-color', 'normal', ''],
        ['scrollbar-3d-light-color', 'normal', ''],
        ['scrollbar-track-color', 'normal', ''],
        ['text-align', 'select', 'left|right|center|justify|initial|inherit'],
        ['text-align-last', 'select', 'auto|left|right|center|justify|start|end|initial|inherit'],
        ['text-decoration', 'select', 'none|underline|overline|line-through|initial|inherit'],
        ['text-indent', 'normal', ''],
        ['text-justify', 'select', 'auto|inter-word|inter-ideograph|inter-cluster|distribute|kashida|trim|initial|inherit'],
        ['text-overflow', 'dynamicSelect', 'clip|ellipsis|initial|inherit'],
        ['text-shadow', 'normal', ''],
        ['text-transform', 'select', 'none|capitalize|uppercase|lowercase|initial|inherit'],
        ['top', 'normal', ''],
        ['vertical-align', 'dynamicSelect', 'baseline|sub|super|top|text-top|middle|bottom|text-bottom|initial|inherit'],
        ['visibility', 'select', 'visible|hidden|collapse|initial|inherit'],
        ['white-space', 'select', 'normal|nowrap|pre|pre-line|pre-wrap|initial|inherit'],
        ['widows', 'normal', ''],
        ['width', 'normal', ''],
        ['word-break', 'select', 'normal|break-all|keep-all|initial|inherit'],
        ['word-spacing', 'normal', ''],
        ['word-wrap', 'select', 'normal|break-word|initial|inherit'],
        ['z-index', 'normal', ''],
        ['zoom', 'normal', '']
    ];
    
    /**
     * sets the element whose properties are going to be edited.
     */
    this.setElementBeingEdited = function (pElement)
    {
        __elementEdited = pElement;
        this.dumpProperties();
    }
    
    /**
     * Clears the html properties section.
     */
    this.clearProperties = function()
    {
        __propertiesSection.find('.property').each(function() {
            $(this).remove();
        });
    }
    
    /**
     * Returns the properties data related to the given property name.
     * It will return an array of two elements: the input type and the input data. 
     * If property isn't found, the array will be void.
     */
    this.__getPropertiesData = function(pPropertyName)
    {
        var lResult = [];
        var lFound = false;
        
        for (i = 0; i < __genericProperties.length && !lFound; i++)
        {
            if (pPropertyName == __genericProperties[i][0])
            {
                lResult.push(__genericProperties[i][1]);
                lResult.push(__genericProperties[i][2]);
                lFound = true;
            }
        }
        
        return lResult;
    }
    
    /**
     * Returns the property index of the given name
     */
    this.__getPropertyIndex = function(pPropertyName)
    {
        var lResult = [];
        var lFound = false;
        var i = 0;
        
        for (i = 0; i < __genericProperties.length && !lFound; i++)
        {
            lFound = (pPropertyName == __genericProperties[i][0])
        }
        
        return i-1;
    }
    
    /**
     * Retrieves the property value from the given property name hold by the selected element.
     */
    this.getPropertyValue = function (pPropertyName)
    {
        if (!__elementEdited)
        {
            return;
        }
        
        return __elementEdited.css(pPropertyName);
    }
    
    /**
     * Returns an html object representing this property.
     * This object can be appended directly to the DOM.
     */
    this.__generateProperty = function(pPropertyIndex, pPropertyData)
    {
        var lPropertyName = pPropertyData[0]
        var lPropertyType = pPropertyData[1];
        var lPropertyValues = pPropertyData[2].split('|');
        // Let's take the property value that the element keeps.
        var lPropertyValue = this.getPropertyValue(lPropertyName);
        
        var lProperty = $('<div id="property_' + pPropertyIndex + '" class="property"></div>');

        var lPropertyCaptionDiv = $('<div class="propertyCaption"></div>');
        var lPropertyCaptionInput = $('<input id="propCaption_' + pPropertyIndex + '" class="propertyCaptionInput" type="text" value="' + lPropertyName + '" readonly></input>');
        
        lPropertyCaptionDiv.width(__splitter.position().left);
        
        var lPropertyValueDiv = $('<div class="propertyValue"></div>');
        var lPropertyValueInput;
        
        lPropertyValueDiv.css('left', __splitter.position().left);
        
        switch (lPropertyType)
        {
            case 'normal':
                lPropertyValueInput = $('<input id="propValue_' + pPropertyIndex + '" class="propertyValueInput" type="text" value="'+ lPropertyValue +'"></div>');
                lPropertyValueInput.change(function()
                {
                    __elementEdited.css(lPropertyName, $(this).val());
                });
                break;
                
            case 'select':
                //lPropertyValueInput = $('<select id="propValue_' + pPropertyIndex + '" class="propertyValueInput"></select>');
                var lInputToWrap = '<select id="propValue_' + pPropertyIndex + '" class="propertyValueInput">';

                for (j=0; j<lPropertyValues.length; j++)
                {
                    var lSelected = '';
                    if (lPropertyValues[j] == lPropertyValue)
                    {
                        lSelected = 'selected="selected"';
                    }
                    lInputToWrap += '<option value="' + lPropertyValues[j] + '" ' + lSelected + '>'+lPropertyValues[j]+'</option>';                    
                }
                
                lInputToWrap += '</select>';
                lPropertyValueInput = $(lInputToWrap);
                
                lPropertyValueInput.change(function()
                {
                    __elementEdited.css(lPropertyName, $(this).find("option:selected").val());
                });
                break;
                
            case 'dynamicSelect':
                var lInputToWrap = '<input id="propValue_' + pPropertyIndex + '" list="'+ lPropertyName +'" name="propValue_' + pPropertyIndex + '" class="propertyValueInput" value="'+lPropertyValue+'">';
                lInputToWrap += '<datalist id="'+ lPropertyName +'">';
                
                for (j=0; j<lPropertyValues.length; j++)
                {
                    var lSelected = '';
                    if (lPropertyValues[j] == lPropertyValue)
                    {
                        lSelected = 'selected="selected"';
                    }
                    lInputToWrap += '<option value="' + lPropertyValues[j] + '" ' + lSelected + '>';//+lPropertyValues[j]+'</option>';                    
                }
                
                lPropertyValueInput = $(lInputToWrap);
                lPropertyValueInput.change(function()
                {
                    __elementEdited.css(lPropertyName, $(this).val());
                });
                break;
        }
        
        
        
        lPropertyCaptionDiv.append(lPropertyCaptionInput);
        lPropertyValueDiv.append(lPropertyValueInput);
        lProperty.append(lPropertyCaptionDiv);
        lProperty.append(lPropertyValueDiv);
        
        return lProperty;
    }
    
    /**
     * Returns the list of properties dynamically generated using the __elementEdited.
     */
    this.__generatePropertiesList = function()
    {
        var lProperties = [];
        for (i = 0; i < __genericProperties.length; i++)
        {
            lProperties.push(this.__generateProperty(i, __genericProperties[i]));
        }
        
        return lProperties;
    }
    
    /**
     * Updates the size and the position of the element.
     */
    this.updateSizeAndPosition = function()
    {
        if (!__elementEdited)
        {
            return;
        }
        
        var lPropertiesToUpdate = [
            'top', 'left', 'width', 'height'
        ];
        
        for (i = 0; i < lPropertiesToUpdate.length; i++)
        {
            var lProperty = lPropertiesToUpdate[i];
            var lPropertyIndex = this.__getPropertyIndex(lProperty);
            
            var lDataContainer = $("#propValue_"+lPropertyIndex);
            lDataContainer.attr('value', this.getPropertyValue(lProperty));
        }
        
    }
    
    /**
     * Dumps all the properties from the selected element into the properties section.
     */
    this.dumpProperties = function()
    {
        var lProperties = this.__generatePropertiesList();
        
        this.clearProperties();
        
        for (i=0; i<lProperties.length; i++)
        {
            __propertiesSection.append(lProperties[i]);
        }
    }
}