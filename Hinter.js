/**
 * Hinter unit v0.3
 * by Iván de Paz Centeno
 * 03/04/2015 
 */

/**
 * Class that handles the elements hints (attached to itself).
 * The hints shows the left, top, width and height of the element.
 */
function Hinter(pBoard)
{
    var __board = pBoard;
    
    /**
     * Element to hint
     */
    var __hintedElement = null;
    
    /**
     * Elements that compose the hint
     */
    var __hintElements = [];
    
    /**
     * Returns the hinted element.
     */
    this.getHintedElement = function()
    {
        return __hintedElement;
    }
    
    /**
     * shows the hint for the specified element
     */
    this.showHint = function(pElement)
    {
        if (this.isAnyElementHinted())
        {
            this.clearHint();
        }
        
        if (!pElement)
        {
            return;
        }
        
        
        __hintedElement = pElement;
        
        //Now we create the hint elements:
        var lHintTop = $("<div id=\"hintElementTop\"></div>");
        var lHintBottom = $("<div id=\"hintElementBottom\"></div>");
        __hintElements.push(lHintTop);
        __hintElements.push(lHintBottom);
        
        for (i = 0; i < __hintElements.length; i++)
        {
            __hintedElement.append(__hintElements[i]);
        }
        
        this.updateHintInfo();
    };
    
    /**
     * clears the hint from the board.
     */
    this.clearHint = function()
    {
        if (!this.isAnyElementHinted())
        {
            return;
        }
        
        __hintedElement = null;
        
        for (i=0; i < __hintElements.length; i++)
        {
            __hintElements[i].remove();
        }
        
        __hintElements = [];
    };
    
    /**
     * Returns true if any element of the board is hightlighted.
     */
    this.isAnyElementHinted = function()
    {
        return __hintedElement != null;
    };
    
    /**
     * Updates the hint information
     */
    this.updateHintInfo = function()
    {
        if (__hintElements.length > 0)
        {
            __hintElements[0].html("<b>" + __hintedElement.data("name") + "</b>");
            
            __hintElements[1].html("<b>Top: </b>" + parseInt(__hintedElement.css("top"))
            + "; <b>Left: </b>" + parseInt(__hintedElement.css("left"))
            + "; <b>Width: </b>" + parseInt(__hintedElement.css("width"))
            + "; <b>Height: </b>" + parseInt(__hintedElement.css("height")));
        }
    };
    
    /**
     * On mouse move event fired we update the values shown in the hint
     */
    this.onMouseMove = function(pMouse)
    {
        if (!this.isAnyElementHinted())
        {
            return;
        }
        
        this.updateHintInfo ();
    }
}