/**
 * TableModifier unit v0.1
 * by Iván de Paz Centeno
 * 06/04/2015 
 */

/**
 * Table modifier class.
 * Allows table cols/rows modifications by click
 */
function TableModifier (pBoard, pTable, pTreeController)
{
    var __board = pBoard;
    var __table = pTable;
    var __self = this;
    var __treeController = pTreeController;
    
    var __trsCount = 0;
    var __tdsCount = 0;
    
    var __allowModification = false;
    
    /**
     * Holds all the attached elements that handles the click events
     */
    var __modificationElements = [];
    
    /**
     * Wraps a table inside this modifier
     */
    this.wrapTable = function (pTable)
    {
        __table = pTable;
    }
    
    /**
     * Decreases the counts by the specified amount
     */
    this.decreaseCounts = function(pBy)
    {
        __trsCount -= pBy;
        __tdsCount -= pBy;
    }
    
    /**
     * Tell us whether table modifications are enabled or not.
     */
    this.isBeingModified = function()
    {
        return __allowModification;
    }
    
    /**
     * Creates a new Table row and returns it to be added to a table.
     */
    this.createTr = function()
    {
        var lId = "TR" + this.createdTrsCount();
        
        var lTr = $("<tr id=\"" + lId + "\"></tr>");
        
        lTr.data("name", "TR" + this.createdTrsCount());
        lTr.data("type", "tr");
        lTr.data("resizable", "false");
        lTr.data("movable", "false");
        
        __trsCount++;
        
        return lTr;
    };
    
    /**
     * Creates a new Table data cell and returns it to be added to a table.
     */
    this.createTd = function()
    {
        var lId = "TD" + this.createdTdsCount();
        
        var lContent = $("<div id=\"Tdcontent"+ this.createdTdsCount() + "\" class=\"tdContent\"></div>");
        
        var lTd = $("<td id=\"" + lId + "\" class=\"defaultTable\"></td>");
        
        lContent.data("type", "tdcontent");
        lContent.data("name", "Tdcontent" + this.createdTdsCount());
        lTd.append(lContent);
        
        lTd.data("name", "TD" + this.createdTdsCount());
        lTd.data("type", "td");
        lTd.data("resizable", "false");
        lTd.data("movable", "false");
        
        
        __tdsCount++;
        
        return lTd;
    };
    
    /**
     * Enable the modifications in the table by spawning the handlers on each side of the table.
     */
    this.enableModification = function()
    {
        if (this.isBeingModified())
        {
            this.disableModification();
        }
        
        __allowModification = true;
        
        // We need to spawn 8 triangles in order to allow modifications. Each two-group on each side of the table.
        __modificationElements = [
            $("<div id=\"topNorthTriangle\" class=\"topGrow\"></div>"),
            $("<div id=\"topSouthTriangle\" class=\"bottomGrow\"></div>"),
            $("<div id=\"leftWestTriangle\" class=\"leftGrow\"></div>"),
            $("<div id=\"leftEastTriangle\" class=\"rightGrow\"></div>"),
            $("<div id=\"bottomNorthTriangle\" class=\"topGrow\"></div>"),
            $("<div id=\"bottomSouthTriangle\" class=\"bottomGrow\"></div>"),
            $("<div id=\"rightWestTriangle\" class=\"leftGrow\"></div>"),
            $("<div id=\"rightEastTriangle\" class=\"rightGrow\"></div>")
        ];
        
        // Let's configure them and spawn to the table
        for (i=0; i <__modificationElements.length; i++)
        {
            __modificationElements[i].data("tableHandler", "true");
            __modificationElements[i].click({param1: i}, this.handleModificationClick);
            __modificationElements[i].data("resizable", "false");
            __modificationElements[i].data("movable", "false");
            __table.append(__modificationElements[i]);
        }
        
        // Now only pends its position:
        __modificationElements[0].css({
            left: 0,
            top: -30,
            height: 15,
            right: 0
        });
        __modificationElements[1].css({
            left: 0,
            top: -15,
            height: 15,
            right: 0
        });
        
        __modificationElements[2].css({
            left: -30,
            top: 0,
            bottom: 0,
            width: 15
        });
        __modificationElements[3].css({
            left: -15,
            top: 0,
            bottom: 0,
            width: 15
        });
        
        __modificationElements[4].css({
            left: 0,
            bottom: -15,
            height: 15,
            right: 0
        });
        
        __modificationElements[5].css({
            left: 0,
            bottom: -30,
            height: 15,
            right: 0
        });
        
        __modificationElements[6].css({
            right: -15,
            top: 0,
            bottom: 0,
            width: 15
        });
        __modificationElements[7].css({
            right: -30,
            top: 0,
            bottom: 0,
            width: 15
        });
    }
    
    /**
     * When a handler receives a click event
     */
    this.handleModificationClick = function(pEvent)
    {
       var lHandlerIndex = pEvent.data.param1;
       
       switch(lHandlerIndex)
       {
           case 0:
               __self.increaseRowOnTop();
               break;
           case 1:
               __self.decreaseRowOnTop();
               break;
           case 2:
               __self.increaseColOnLeft();               
               break;
           case 3:
               __self.decreaseColOnLeft();
               break;
           case 4:
               __self.decreaseRowOnBottom();
               break;
           case 5:
               __self.increaseRowOnBottom();
               break;
           case 6:
               __self.decreaseColOnRight();
               break;
           case 7:
               __self.increaseColOnRight();
               break;
       }
       
       __treeController.updateTree();
    }
    
    /**
     * Disables the modification on the table by despawning the handlers.
     */
    this.disableModification = function()
    {
        if (!this.isBeingModified())
        {
            return;
        }
        
        __allowModification = false;
        
        for (i = 0 ; i < __modificationElements.length; i++)
        {
            __modificationElements[i].remove();
        }
        
        __modificationElements = [];
    }
    
    /**
     * Returns the amount of created rows.
     */
    this.createdTrsCount = function()
    {
        return __trsCount;
    }
    
    /**
     * Returns the amount of created table data cells.
     */
    this.createdTdsCount = function()
    {
        return __tdsCount;
    }
    
    /**
     * Returns the amount of rows from the wrapped table.
     */
    this.rowCount = function()
    {
        return $("#" + __table.attr('id') +" > tbody > tr:last").index() + 1;
    }
    
    /**
     * Returns the amount of cols from the wrapped table.
     */
    this.colCount = function()
    {
        return $("#" + __table.attr('id') +" > tbody").find("> tr:first > td").length;
    }
    
    /**
     * Adds a row in the top of the table
     */
    this.increaseRowOnTop = function()
    {
        var lColCount= $("#" + __table.attr('id') +" > tbody").find("> tr:first > td").length
        var lRow = this.createTr();
        
        for (i = 0; i < lColCount; i++)
        {
            lRow.append(this.createTd());
        }
        
        __table.prepend(lRow);
    }
    
    /**
     * Adds a row in the top of the table
     */
    this.increaseRowOnBottom = function()
    {
        var lColCount= this.colCount();
        var lRow = this.createTr();
        
        for (i = 0; i < lColCount; i++)
        {
            lRow.append(this.createTd());
        }
        
        __table.append(lRow);
    }
    
    /**
     * removes a row in the top of the table
     */
    this.decreaseRowOnTop = function()
    {
        if (this.rowCount() == 1)
        {
            return;
        }
        
        $("#" + __table.attr('id') +"> tbody > tr:first").remove();
    }
    
    /**
     * Removes a row in the bottom of the table
     */
    this.decreaseRowOnBottom = function()
    {
        if (this.rowCount() == 1)
        {
            return;
        }
        
        $("#" + __table.attr('id') +"> tbody > tr:last").remove();
    }
    
    /**
     * Adds a col to the right of the table
     */
    this.increaseColOnRight = function()
    {
        $("#" + __table.attr('id') +"> tbody > tr").each(function() {
            $(this).append( __self.createTd() );
        });
    }
    
    /**
     * Removes a col from the right of the table
     */
    this.decreaseColOnRight = function()
    {
        if (this.colCount() == 1)
        {
            return;
        }
        
        $("#" + __table.attr('id') +"> tbody > tr").each(function() {
            $("#"+$(this).attr('id')+" > td:last").remove();
        });
    }
    
    /**
     * Adds a col to the right of the table
     */
    this.increaseColOnLeft = function()
    {
        $("#" + __table.attr('id') +"> tbody > tr").each(function() {
            $(this).prepend( __self.createTd() );
        });
    }
    
    /**
     * Removes a col from the left of the table
     */
    this.decreaseColOnLeft = function()
    {
        if (this.colCount() == 1)
        {
            return;
        }
        
        $("#" + __table.attr('id') +"> tbody > tr").each(function() {
            $("#"+$(this).attr('id')+" > td:first").remove();
        });
    }
    
}