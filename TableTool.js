/**
 * TableTool unit v0.1
 * by Iván de Paz Centeno
 * 05/04/2015 
 */

/**
 * Table Tool class.
 * Represent a tool that allows the creation of Tables
 * on the board.
 */
function TableTool(pBoard, pHighlighter, pHinter, pTreeController, pTableModifier)
{
    var __toolType = 1;
    
    var __tablesCount = 0;
    var __tbodiesCount = 0;
    
    var __board = pBoard;
    var __highlighter = pHighlighter;
    var __hinter = pHinter;
    var __treeController = pTreeController;
    var __tableModifier = pTableModifier;
    
    var __gridSize = 8;
    
    var __createdTableTop = 0;
    var __createdTableLeft = 0;
    
    
    var __tableBeingCreated = null;
    
    var __scrollbarWidth = calculateScrollBarWidth();
    
    /**
     * Updates the grid size of the board
     */
    this.updateGridSize = function (pGridSize)
    {
        __gridSize = pGridSize;
    }
    
    /**
     * Calculates the scrollbar width in pixels
     */
    function calculateScrollBarWidth()
    {
        // Create the measurement node
        var scrollDiv = document.createElement("div");
        scrollDiv.className = "scrollbar-measure";
        document.body.appendChild(scrollDiv);
        
        // Get the scrollbar width
        var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
        
        // Delete the DIV 
        document.body.removeChild(scrollDiv);   
        
        return scrollbarWidth;
    }
    
    /**
     * Returns the current type of the tool
     */
    this.getType = function()
    {
        return __toolType;
    };
    
    /**
     * Returns the name of the current tool
     */
    this.getTypeName = function()
    {
        return Object.keys(ToolTypes)[__toolType];
    };
    
    
    /**
     * Creates and returns an html tool object representation.
     */
    this.getToolIcon = function()
    {
        return $("<div id=\"newTableIcon\"></div>");
    };
    
    
    /**
     * Returns the amount of created table bodies.
     */
    this.createdTableBodiesCount = function()
    {
        return __tbodiesCount;
    }
    
    /**
     * Returns the amount of created Tables by this tool
     */
    this.createdTablesCount = function()
    {
        return __tablesCount;
    };
    
    /**
     * Creates a new Table and returns it to be added to some DOM
     */
    this.createTable = function()
    {
        var lId = "Table" + this.createdTablesCount();
        
        //var lTableDiv = $("<div id=\"Div" + lId + "\" class=\"abs selectedItem\"></div>");
        
        var lTable = $("<table id=\"" + lId + "\" class=\"abs selectedItem defaultTable\"></table>");
        
        var lTableBody = this.createTableBody();
        
        var lTableRow = [
            __tableModifier.createTr(),
            __tableModifier.createTr()
        ];
        
        var lTableData = [
            __tableModifier.createTd(),
            __tableModifier.createTd(),
            __tableModifier.createTd(),
            __tableModifier.createTd()
        ];
        
        lTable.data("name", "Table" + this.createdTablesCount());
        lTable.data("type", "table");
        
        lTable.append (lTableBody);
        
        lTableBody.append(lTableRow[0]);
        lTableBody.append(lTableRow[1]);
        
        lTableRow[0].append(lTableData[0]);
        lTableRow[0].append(lTableData[1]);
        lTableRow[1].append(lTableData[2]);
        lTableRow[1].append(lTableData[3]);
        
        __tablesCount++;
        
        return lTable;
    };
    
    /**
     * Creates a new Table body and returns it to be added to a table.
     */
    this.createTableBody = function()
    {
        var lId = "TBody" + this.createdTableBodiesCount();
        
        var lTableBody = $("<tbody id=\"" + lId + "\"></tbody>");
        
        lTableBody.data("name", "TBody" + this.createdTableBodiesCount());
        lTableBody.data("type", "tbody");
        lTableBody.data("resizable", "false");
        lTableBody.data("movable", "false");
        
        __tbodiesCount++;
        
        return lTableBody;
    };
    

    
    /**
     * Checks if the tool is actually creating a new div or not.
     */
    this.isCreatingTable = function()
    {
        return __tableBeingCreated != null;
    };
    
    
    /**
     * Stops the DIV creation process (if in progress).
     */
    this.cancelCreation = function()
    {
        if (!this.isCreatingTable())
        {
            return;
        }
        
        __hinter.clearHint();
        __tableBeingCreated.remove();
        __tableBeingCreated = null;
        __tablesCount--;
        __tbodiesCount--;
        __tableModifier.decreaseCounts(1);
    };
    
    /**
     * Event called whenever a new mouse down event is raised.
     * This tool must be selected to receive events.
     */
    this.toolMouseDown = function(pMouse)
    {
        if (pMouse.isRightMB())
        {
            return;
        }
        
        __tableBeingCreated = this.createTable ();
        __hinter.showHint(__tableBeingCreated);
        
        var lHolder;
        
        // Let's check if it's on the scrollbar
        if (pMouse.getVisibleCoords().GetX() - __board.offset().left > __board.width() - __scrollbarWidth)
            return;
        
        if (pMouse.getVisibleCoords().GetY() - __board.offset().top > __board.height() - __scrollbarWidth)
            return;
        
        if (__highlighter.isAnyElementHighlighted())
        {
            lHolder = __highlighter.getHightlightedElement();
            
            __createdTableTop = getNearestValueToMultipleOf(pMouse.getVisibleCoords().GetY() - lHolder.offset().top + lHolder.scrollTop(), __gridSize);
            __createdTableLeft = getNearestValueToMultipleOf(pMouse.getVisibleCoords().GetX() - lHolder.offset().left + lHolder.scrollLeft(), __gridSize);
        }
        else   
        {
            lHolder = __board;
            
            __createdTableTop = getNearestValueToMultipleOf(pMouse.getCoords(__board).GetY(), __gridSize);
            __createdTableLeft = getNearestValueToMultipleOf(pMouse.getCoords(__board).GetX(), __gridSize);
        }
        
        
        //We want to keep the actual highlighted element for the user to know WHERE is he placing the new div.
        __highlighter.finishHighlightOfElements(true);
        
        lHolder.append (__tableBeingCreated);
        
        __tableBeingCreated.css({
            top: __createdTableTop,
            left: __createdTableLeft,
            width: getNearestValueToMultipleOf(5, __gridSize),
                              height: getNearestValueToMultipleOf(5, __gridSize)
        });
        
    };
    
    /**
     * Event called whenever a new mouse move event is raised.
     * This tool must be selected to receive events.
     */
    this.toolMouseMove = function(pMouse)
    {
        if (this.isCreatingTable())
        {
            var lWidth;
            var lHeight;
            if (__highlighter.isAnyElementHighlighted())
            {
                var lHolder = __highlighter.getHightlightedElement();
                lWidth =  pMouse.getVisibleCoords().GetX() - lHolder.offset().left + lHolder.scrollLeft() - __createdTableLeft;
                lHeight = pMouse.getVisibleCoords().GetY() - lHolder.offset().top + lHolder.scrollTop() - __createdTableTop;
            }
            else
            {
                lWidth = pMouse.getCoords(__board).GetX() - __createdTableLeft;
                lHeight = pMouse.getCoords(__board).GetY() - __createdTableTop;
            }
            
            lWidth = getNearestValueToMultipleOf(lWidth, __gridSize);
            lHeight = getNearestValueToMultipleOf(lHeight, __gridSize);
            
            
            if (lWidth < 0)
            {
                var lLeft;
                var lNewWidth;
                
                if (__highlighter.isAnyElementHighlighted())
                {
                    lLeft = pMouse.getVisibleCoords().GetX() - __highlighter.getHightlightedElement().offset().left;
                } 
                else
                {
                    lLeft = pMouse.getVisibleCoords().GetX() + __board.scrollLeft() - __board.offset().left;
                }
                
                lLeft = getNearestValueToMultipleOf(lLeft, __gridSize);
                var lNewWidth = getNearestValueToMultipleOf(__createdTableLeft - lLeft, __gridSize);
                __tableBeingCreated.css({
                    left: lLeft,
                    width: lNewWidth
                });
            } 
            else
            {
                __tableBeingCreated.css({                            
                    width: lWidth
                });
            }
            
            if (lHeight < 0)
            {
                var lTop;
                
                if (__highlighter.isAnyElementHighlighted())
                {
                    lTop = pMouse.getVisibleCoords().GetY() - __highlighter.getHightlightedElement().offset().top;
                } 
                else
                {
                    lTop = pMouse.getVisibleCoords().GetY() - __board.offset().top + __board.scrollTop();
                }
                
                lTop = getNearestValueToMultipleOf(lTop, __gridSize);
                
                var lNewHeight = getNearestValueToMultipleOf(__createdTableTop - lTop, __gridSize);
                __tableBeingCreated.css({
                    top: lTop,
                    height: lNewHeight
                });
            }
            else
            {
                __tableBeingCreated.css({
                    height: lHeight
                });
            }
            
            __hinter.onMouseMove (pMouse);
        }
    };
    
    /**
     * Event called whenever a new mouse up event is raised.
     * This tool must be selected to receive events.
     */
    this.toolMouseUp = function(pMouse)
    {
        var lIsRightMB = pMouse.isRightMB();
        
        if (this.isCreatingTable() && !lIsRightMB)
        {
            if (__tableBeingCreated.width() == 0 || __tableBeingCreated.height() == 0)
            {
                this.cancelCreation();
            }
            else
            {
                __tableBeingCreated.removeClass('selectedItem');
                __tableBeingCreated = null;
                __treeController.updateTree();
            }
            __highlighter.cancelHightlight ();
            __hinter.clearHint();
        }
        __highlighter.initHighlightOfElements ();
    };
}