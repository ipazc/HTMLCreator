/**
 * DivTool unit v0.3
 * by Iván de Paz Centeno
 * 05/04/2015 
 */

/**
 * Div Tool class.
 * Represent a tool that allows the creation of DIVs
 * on the board.
 */
function DivTool(pBoard, pHighlighter, pHinter, pTreeController)
{
    var __toolType = 0;
    
    var __divsCount = 0;
    var __board = pBoard;
    var __highlighter = pHighlighter;
    var __hinter = pHinter;
    var __treeController = pTreeController;
    
    var __gridSize = 8;
    
    var __createdDivTop = 0;
    var __createdDivLeft = 0;
    
    
    var __divBeingCreated = null;
    
    var __scrollbarWidth = calculateScrollBarWidth();
    
    /**
     * Updates the grid size of the board
     */
    this.updateGridSize = function (pGridSize)
    {
        __gridSize = pGridSize;
    }
    
    /**
     * Calculates the scrollbar width in pixels
     */
    function calculateScrollBarWidth()
    {
        // Create the measurement node
        var scrollDiv = document.createElement("div");
        scrollDiv.className = "scrollbar-measure";
        document.body.appendChild(scrollDiv);
        
        // Get the scrollbar width
        var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
        
        // Delete the DIV 
        document.body.removeChild(scrollDiv);   
        
        return scrollbarWidth;
    }
    
    /**
     * Returns the current type of the tool
     */
    this.getType = function()
    {
        return __toolType;
    };
    
    /**
     * Returns the name of the current tool
     */
    this.getTypeName = function()
    {
        return Object.keys(ToolTypes)[__toolType];
    };
    
    
    /**
     * Creates and returns an html tool object representation.
     */
    this.getToolIcon = function()
    {
        return $("<div id=\"newDivIcon\"></div>");
    };
    
    /**
     * Returns the amount of created divs by this tool
     */
    this.createdDivsCount = function()
    {
        return __divsCount;
    };
    
    /**
     * Creates a new DIV and returns it to be added to some DOM
     */
    this.createDiv = function()
    {
        var lDiv = $("<div id=\"Div" + this.createdDivsCount() + "\" class=\"abs selectedItem\"></div>");
        
        lDiv.data("name", "Div" + this.createdDivsCount());
        lDiv.data("type", "div");
        
        __divsCount++;
        
        return lDiv;
    };
    
    
    /**
     * Checks if the tool is actually creating a new div or not.
     */
    this.isCreatingDiv = function()
    {
        return __divBeingCreated != null;
    };
    
    
    /**
     * Stops the DIV creation process (if in progress).
     */
    this.cancelCreation = function()
    {
        if (!this.isCreatingDiv())
        {
            return;
        }
        
        __hinter.clearHint();
        __divBeingCreated.remove();
        __divBeingCreated = null;
        __divsCount--;
    };
    
    /**
     * Event called whenever a new mouse down event is raised.
     * This tool must be selected to receive events.
     */
    this.toolMouseDown = function(pMouse)
    {
        if (pMouse.isRightMB())
        {
            return;
        }
        
        __divBeingCreated = this.createDiv ();
        __hinter.showHint(__divBeingCreated);
        
        var lHolder;
        
        // Let's check if it's on the scrollbar
        if (pMouse.getVisibleCoords().GetX() - __board.offset().left > __board.width() - __scrollbarWidth)
            return;
        
        if (pMouse.getVisibleCoords().GetY() - __board.offset().top > __board.height() - __scrollbarWidth)
            return;
        
        if (__highlighter.isAnyElementHighlighted())
        {
            lHolder = __highlighter.getHightlightedElement();
            
            __createdDivTop = getNearestValueToMultipleOf(pMouse.getVisibleCoords().GetY() - lHolder.offset().top + lHolder.scrollTop(), __gridSize);
            __createdDivLeft = getNearestValueToMultipleOf(pMouse.getVisibleCoords().GetX() - lHolder.offset().left + lHolder.scrollLeft(), __gridSize);
        }
        else   
        {
            lHolder = __board;
            
            __createdDivTop = getNearestValueToMultipleOf(pMouse.getCoords(__board).GetY(), __gridSize);
            __createdDivLeft = getNearestValueToMultipleOf(pMouse.getCoords(__board).GetX(), __gridSize);
        }
        
        
        //We want to keep the actual highlighted element for the user to know WHERE is he placing the new div.
        __highlighter.finishHighlightOfElements(true);
        
        lHolder.append (__divBeingCreated);
        
        __divBeingCreated.css({
            top: __createdDivTop,
            left: __createdDivLeft,
            width: getNearestValueToMultipleOf(5, __gridSize),
                              height: getNearestValueToMultipleOf(5, __gridSize)
        });
        
    };
    
    /**
     * Event called whenever a new mouse move event is raised.
     * This tool must be selected to receive events.
     */
    this.toolMouseMove = function(pMouse)
    {
        if (this.isCreatingDiv())
        {
            var lWidth;
            var lHeight;
            if (__highlighter.isAnyElementHighlighted())
            {
                var lHolder = __highlighter.getHightlightedElement();
                lWidth =  pMouse.getVisibleCoords().GetX() - lHolder.offset().left + lHolder.scrollLeft() - __createdDivLeft;
                lHeight = pMouse.getVisibleCoords().GetY() - lHolder.offset().top + lHolder.scrollTop() - __createdDivTop;
            }
            else
            {
                lWidth = pMouse.getCoords(__board).GetX() - __createdDivLeft;
                lHeight = pMouse.getCoords(__board).GetY() - __createdDivTop;
            }
            
            lWidth = getNearestValueToMultipleOf(lWidth, __gridSize);
            lHeight = getNearestValueToMultipleOf(lHeight, __gridSize);
            
            
            if (lWidth < 0)
            {
                var lLeft;
                var lNewWidth;
                
                if (__highlighter.isAnyElementHighlighted())
                {
                    lLeft = pMouse.getVisibleCoords().GetX() - __highlighter.getHightlightedElement().offset().left;
                } 
                else
                {
                    lLeft = pMouse.getVisibleCoords().GetX() + __board.scrollLeft() - __board.offset().left;
                }
                
                lLeft = getNearestValueToMultipleOf(lLeft, __gridSize);
                var lNewWidth = getNearestValueToMultipleOf(__createdDivLeft - lLeft, __gridSize);
                __divBeingCreated.css({
                    left: lLeft,
                    width: lNewWidth
                });
            } 
            else
            {
                __divBeingCreated.css({                            
                    width: lWidth
                });
            }
            
            if (lHeight < 0)
            {
                var lTop;
                
                if (__highlighter.isAnyElementHighlighted())
                {
                    lTop = pMouse.getVisibleCoords().GetY() - __highlighter.getHightlightedElement().offset().top;
                } 
                else
                {
                    lTop = pMouse.getVisibleCoords().GetY() - __board.offset().top + __board.scrollTop();
                }
                
                lTop = getNearestValueToMultipleOf(lTop, __gridSize);
                
                var lNewHeight = getNearestValueToMultipleOf(__createdDivTop - lTop, __gridSize);
                __divBeingCreated.css({
                    top: lTop,
                    height: lNewHeight
                });
            }
            else
            {
                __divBeingCreated.css({
                    height: lHeight
                });
            }
            
            __hinter.onMouseMove (pMouse);
        }
    };
    
    /**
     * Event called whenever a new mouse up event is raised.
     * This tool must be selected to receive events.
     */
    this.toolMouseUp = function(pMouse)
    {
        var lIsRightMB = pMouse.isRightMB();
        
        if (this.isCreatingDiv() && !lIsRightMB)
        {
            if (__divBeingCreated.width() == 0 || __divBeingCreated.height() == 0)
            {
                this.cancelCreation();
            }
            else
            {
                __divBeingCreated.removeClass('selectedItem');
                __divBeingCreated = null;
                __treeController.updateTree();
            }
            __highlighter.cancelHightlight ();
            __hinter.clearHint();
        }
        __highlighter.initHighlightOfElements ();
    };
}